let result = null

const mapObject = (testObject,callbackFUnction) => {

    if (Object.keys(testObject).length !==0) {

        for (let objectKey of Object.keys(testObject)) {

            result = callbackFUnction(objectKey,testObject[objectKey])
        }

        return result

    } else {

        return "The object you passed as an argument to this function is empty."
    }
}

module.exports = mapObject