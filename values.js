const arrayOfTestObjectValues = []

const values = (testObject) => {
    if (Object.values(testObject).length !== 0) {
        for (let objectValue of Object.values(testObject)) {
            arrayOfTestObjectValues.push(objectValue)
        }
        return arrayOfTestObjectValues
    } else {
        return "The object you passed as an argument to this function is empty."
    }
}


module.exports = values