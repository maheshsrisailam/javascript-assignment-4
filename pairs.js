const arrayOfTestObjectPairs = []

const pairs = (testObject) => {
   if (Object.keys(testObject).length !== 0) {
      for (let objectKey of Object.keys(testObject)) {
         arrayOfTestObjectPairs.push([objectKey,testObject[objectKey]])
      }
      return arrayOfTestObjectPairs
   } else {
      return "The object you passed as an argument to this function is empty."
   }
}


module.exports = pairs