const invertedTestObject = {}

const invert = (testObject) => {
   if (Object.keys(testObject).length !==0) {
      for (let objectKey of Object.keys(testObject)) {
         invertedTestObject[testObject[objectKey]] = objectKey
    }
    return invertedTestObject
   } else {
      return "The object you passed as an argument to this function is empty."
   }
}


module.exports = invert