const invert = require('../invert')
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }

const invertedTestObject = invert(testObject)
console.log(invertedTestObject)