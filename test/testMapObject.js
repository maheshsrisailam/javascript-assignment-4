const mapObject = require('../mapObject')

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const newObject = {name: undefined, age: undefined, location: undefined}

const callbackFunction = (key,value) => {
    
    newObject[key] = value + " javascript"

    return newObject
        
}
const resultOfMapObject = mapObject(testObject,callbackFunction)

console.log(resultOfMapObject)