const defaults = (testObject, defaultObject) => {
    if (Object.keys(testObject).length !== 0) {
        for (let objectKey of Object.keys(testObject)) {
            if (testObject[objectKey] === undefined) {
                testObject[objectKey] = defaultObject[objectKey]
            }
       }
        return testObject
    } else {
        return "The object you passed as an argument to this function is empty"
    }
}

module.exports = defaults