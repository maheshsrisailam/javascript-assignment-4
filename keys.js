const arrayOfTestObjectKeys = []

const keys = (testObject) => {
    if (Object.keys(testObject).length !== 0) {
        for (let objectKey of Object.keys(testObject)) {
            arrayOfTestObjectKeys.push(objectKey)
        }
        return arrayOfTestObjectKeys
    } else {
        return 'The object you passed as an argument to this function is empty.'
    }
    
}


module.exports = keys